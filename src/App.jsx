/** @jsxImportSource @emotion/react */
import { jsx, css } from "@emotion/react";
import BugsPage from '@/pages/BugsPage.jsx';

const App = () => {
    const compName = 'app';

    const styles = {
        app: {
            display: 'grid',
            placeItems: 'center',
            width: '100vw',
            height: '100vh',
            backgroundColor: '#e0ffe5'
        }
    };

    return (
        <div className={compName} css={styles.app}>
            <BugsPage />
        </div>
    )
}

export default App;
