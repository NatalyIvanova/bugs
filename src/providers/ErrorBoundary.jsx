import React from 'react';

class CustomErrorBoundary extends React.Component {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
        this.reset = this.reset.bind(this);
        this.defaultError = 'An unexpected error occurred. Please, contact your administrator.'
    }

    static getDerivedStateFromError(error) {
        return { hasError: true };
    }

    reset() {
        this.setState({ hasError: false });
    }

    render() {
        if (this.state.hasError) {
            return (
                <div onClick={this.reset}>
                    {this.props.fallback ? this.props.fallback : this.defaultError}
                </div>
            )
        }
        return this.props.children;
    }
}

export default CustomErrorBoundary;