import {useState} from 'react';
import IconButton from '@mui/material/IconButton';
import {Box} from '@mui/material';

const styles = {
    iconCont: {
        display: 'grid',
        placeItems: 'center',
        textAlign: 'center',
        fontSize: '1.2rem',
        lineHeight: 1.2,
        width: '190px',
        height: '190px'
    },
    icon: {
        display: 'block',
        width: '140px',
        height: '140px',
        marginBottom: 2
    }
};


const Flower = ({ children, label }) => {
    const compName = 'flower';
    const [isError, setError] = useState(false);

    const handleErrorClicked = () => {
        setError(true);
    }

    if (isError) {
        throw new Error('A bug is released!');
    } else {
        return (
            <Box className={compName} sx={styles.iconCont}>
                <IconButton onClick={handleErrorClicked} sx={styles.icon}>
                    { children }
                </IconButton>
                { label }
            </Box>
        )
    }
}

export default Flower;