/** @jsxImportSource @emotion/react */
import { jsx, css } from "@emotion/react";
import { Box, Button } from '@mui/material';
import { ReactComponent as BugSvg } from "../../assets/bugs/bug.svg";

const styles = {
    iconCont: {
        display: 'grid',
        placeItems: 'center',
        width: '190px',
        height: '190px'
    },
    icon: {
        display: 'block',
        width: '120px',
        height: '120px',
        marginBottom: '20px'
    }
};

export const Bug = ({resetErrorBoundary}) => {
    const compName = 'bug';

    return (
        <Box className={compName} sx={styles.iconCont}>
            <BugSvg css={styles.icon} />
            <Button onClick={resetErrorBoundary} color='error'>
                Remove Bug
            </Button>
        </Box>
    )
}