import { useState } from 'react';
import {Box, Button} from '@mui/material';
import { ErrorBoundary } from 'react-error-boundary';
import Flower from '@/components/bugs/Flower.jsx';
import {Bug} from '@/components/bugs/Bug.jsx';
import CustomErrorBoundary from '@/providers/ErrorBoundary.jsx';
import { ReactComponent as GreenFlowerSvg } from "../assets/bugs/flower_green.svg";
import { ReactComponent as PinkFlowerSvg } from "../assets/bugs/flower_pink.svg";
import { ReactComponent as YellowFlowerSvg } from "../assets/bugs/flower_yellow.svg";

const styles = {
    cont: {
        display: 'flex',
        justifyContent: 'center',
        gap: 10
    }
}

const BugPage = () => {
    const compName = 'bug-page';
    const [isError, setError] = useState(false);

    if (isError) {
        throw new Error('A custom error is released!');
    } else {
        return (
            <Box className={compName} sx={styles.cont}>
                <Flower label='No Error Boundary (Do not forget to reload)'>
                    <GreenFlowerSvg />
                </Flower>
                <CustomErrorBoundary fallback={<Bug />}>
                    <Flower label='Custom Error Boundary'>
                        <PinkFlowerSvg />
                    </Flower>
                </CustomErrorBoundary>
                <ErrorBoundary FallbackComponent={Bug}>
                    <Flower label='react-error-boundary'>
                        <YellowFlowerSvg />
                    </Flower>
                </ErrorBoundary>
            </Box>
        )
    }
}

export default BugPage;